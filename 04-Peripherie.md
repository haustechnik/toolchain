# Peripherie

Ein Mikrocontroller hat häufig eine Vielzahl von Peripheriekomponenten, mit welchen er mit der Außenwelt interagieren kann.
Zum einen sind dies üblicherweise Systeme zur Ein- oder Ausgabe von digitalen oder analogen Signalen auf einzelnen Pins.
Zum anderen sind dies häufig Busse oder andere digitale Kommunikationssysteme zur Interaktion mit anderen Prozessoren.
Eine vollständige Auflistung als Übersicht ist im [Datenblatt][] zu finden, hier sollen nur die für diesen Anwendungsfall wichtigsten Systeme kurz erläutert werden.

Der Vollständigkeit halber werden neben tatsächlichen Peripheriekomponenten auch einige Blöcke betrachet, welche im engsten Sinne nicht zur Peripherie gehören.

  [Datenblatt]: res/L051/STM32L051R8.pdf
  [Reference Manual]: res/L051/RM0377%20-%20Ultra-low-power%20STM32L0x1%20advanced%20ARM-based%2032-bit%20MCUs.pdf

## Speicher

Die Cortex-M0+ Architektur ist eine von Neumann-Architektur, besitzt also einen gemeinsamen Programm- und Datenspeicher. Der Speicher besteht aus drei verschiedenen physikalischen Speichertechnologien, eingebunden über einen gemeinsamen Bus und Adressraum. Die verschiedenen Speicherbereiche sind in Kapitel 2.2 des [Reference Manual][] beschrieben.

### RAM

Ein STM32L051xx verfügt über 8 KB RAM in [SRAM](https://en.wikipedia.org/wiki/Static_random-access_memory)-Ausführung. Das RAM ist sehr schnell und lässt sich auch bei maximaler Taktfrequenz innerhalb eines Zyklus lesen. Der Zugriff kann in Bytes, _half-words_ (16 Bit) oder _full words_ (32 Bit) erfolgen.

### Flash

Controller der STM32L051x8-Reihe besitzen 64 KB Flash-Speicher, aufgeteilt in 512 _Pages_ von je 128 _Bytes_ bzw. 32 _Words_. Dieser Speicher ist primär gedacht als Programmspeicher, es lassen sich jedoch natürlich auch Anwendungsdaten darin speichern. Auch im Flash-Speicher lasse sich Daten in 8-, 16- oder 32-Bit Blöcken auslesen.
Vor dem Schreiben des Flash-Speichers muss erst die betreffende _Page_ komplett gelöscht werden, anschließend lassen sich _Half Pages_ (16 Byte) am Stück oder einzelne _Words_ beschreiben. Die verschiedenen Operationen sind in den Abschnitten 3.3.3 und 3.3.4 des [Reference Manual][] im Detail beschrieben.

### EEPROM

Zusätzlich zum primär für Programmcode gedachten Flash-Speicher sind im Speicher der Controller der SMT32L051xx-Familie noch 2 KB EEPROM _(electrically erasable programmable read-only memory)_ enthalten. Dieser Speicher ist wie der Flash-Speicher nicht flüchtig (bleibt bei Spannungsverlust bestehen), lässt sich jedoch Byte-weise beschreiben und Word-weise löschen. Durch die feinere Auflösung eignet sich der Speicher besonders für sich gelegentlich ändernde Konfigurationseinstellungen und ähnliche von der Anwendung selbst geschriebene Daten.

In Kapitel 3 _"Flash program memory and data EEPROM"_ des [Reference Manual][] sind die beiden nicht flüchtigen Speicher des Mikrocontrollers im Detail beschrieben.

## GPIOs

Jeder Pin (bis auf die Spannungsversorgung) lässt sich als digitaler _General Purpose Input/Output_ (GPIO) verwenden. Ein Output kann in _push-pull_ Konfiguration mit dominanten Pegeln oder als _open drain + pull up/down_ mit rezessiven Pegeln betrieben werden. Zu einem als Input konfigurerten Pin lassen sich ebenfalls optional pull-up- oder pull-down-Widerstände dazu schalten.
Kapitel 8 im [Reference Manual][] deckt die GPIO-Konfiguration ab.

## ADC

Die Mikrocontroller der STM32L051xx-Reihe haben einen internen 12-bit Analog-Digital-Wandler, welcher durch einen internen Multiplexer nacheinander bis zu 16 Kanäle aufnehmen kann. Die Auflösung ist sowohl zu Gunsten der Umwandlungszeit nach unten korrigierbar als auch durch Oversampling nach oben erweiterbar. In [AN4629](res/L051/AN4629 - ADC hardware oversampling for microcontrollers of the STM32 L0 series.pdf) werden die Vor- und Nachteile des _Hardware Oversampling_ etwas detaillierter diskutiert.

Der ADC lässt sich selbstverständlich in vielen verschiedenen Modi betreiben, welche alle in Kapitel 13 des [Reference Manual][] beschrieben sind. Besonders interessant ist der [DMA](#DMA)-Support, wodurch Daten nach erfolgter Konvertierung direkt in den Speicher kopieren lassen, ohne dass hierfür Prozessorzeit verwendet werden muss.

## Timer & PWM

Eine Zeitbasis lässt sich generell durch einen Zähler in Verbindung mit einer Taktquelle erzeugen.
Für (unter anderem) hat der Controller für diesen Zweck verschiedene in Hardware implementierte Zähler. Diese lassen sich in Software bezüglich Taktquelle, _Prescaler_, Zählrichtung und Obergrenze konfigurieren, laufen jedoch unabhängig vom Prozessor.
Verbindet man so einen Zähler mit einer Schaltschwelle, erhält man eine Pulsbreitenmodulation _(Pulse Width Modulation, PWM)_ auf einem Output-Pin. Auf der verwendeten Hardware werden die vier PWM-Kanäle des Timers `TIM2` über ein Tiefpassfilter als analoge Ausgänge (mit niedriger Bandbreite) verwendet, `TIM6` liefert die Zeitbasis für das Betriebssystem.
Eine Übersicht über verfügbare Timer (Zähler) ist im [Datenblatt][] in Abschnitt 3.15 zu finden, detaillierte Beschreibungen sind im [Reference Manual][] in den Kapiteln 16 bis 19 zu finden.

## USART/LPUART & RS-485

Der _Universal Synchronous Asynchronous Receiver/Transmitter (USART)_ ist eine allgemeine serielle Full-Duplex Schnittstelle, welche sich für viele verschiedene Protokolle konfigurieren lässt. Die Implementierung umfasst lediglich die [Bitübertragungsschicht](https://en.wikipedia.org/wiki/Physical_layer) des [OSI-Modells](https://en.wikipedia.org/wiki/OSI_model), optional mit einem Paritätsbit pro Symbol. Ein Symbol kann in der vorliegenden Implementierung aus sieben, acht oder neun Bits bestehen. Die Übertragung erfolgt entweder _mit_ einer Taktleitung (synchron) oder ohne eine Taktleitung (asynchron).

Der _Low Power Universal Asynchronous Receiver/Transmitter (LPUART)_ verfügt über keine Taktleitung, lässt sich aber in einem speziellen Low-Power Betriebsmodus des Controllers betreiben, in welchem die internen Oszillatoren abgeschaltet sind.

In der Bitübertragungsschicht entspricht das RS-485 Protokoll der allgemeinen UART-Spezifikation, sodass für die Implementierung lediglich ein Transceiver erforderlich ist. Da für den betrachteten Einsatzfall ein RS-485 Bus in Half-Duplex-Konfiguration eingeplant ist, ist neben den Datenleitungen noch ein _Driver Enable_-Signal (quasi eine push-to-talk-Taste) notwendig.
Die Kapitel 24 und 25 im [Reference Manual][] beschreiben die USART bzw. LPUART-Schnittstelle im Detail.

## SPI

Ein anderer serieller synchroner Full-Duplex Bus ist das _Serial Peripheral Interface (SPI)_, welches vornehmlich für schnelle Kommunikation mit Peripheriekomponenten auf der gleichen Platine verwendet wird. Aufgrund der kurzen Leitungswege lassen sich häufig Übertragungsraten von mehreren Mbit/s erreichen.
Die Dokumentation der Schnittstelle ist in Kapitel 26 des [Reference Manual][] zu finden.

## DMA

Viele periphere Komponenten können unabhängig vom Prozessor laufen. Damit der Prozessor jedoch nicht durchgängig als Bindeglied zwischen Peripherie und Speicher fungieren muss, lässt sich für diese Zwecke der _Direct Memory Access (DMA) Controller_ konfigurieren. Dieses System hat unabhängig vom Prozessor Zugriff auf den Speicher und kann sowohl vom Speicher zu Peripheriekomponenten als auch umgekehrt Daten transportieren. So lässt sich beispielsweise ein großer Sendebuffer ohne den Prozessor zu belasten Byte-weise über eine serielle Schnittstelle übertragen. Auch ist es möglich, in Echtzeit Daten auf einer Schnittstelle entgegenzunehmen und per DMA im Speicher zu puffern, bis der Prozessor einen Datenblock am Stück verarbeiten kann.

Die verschiedenen Konfigurations- und Betriebsmöglichkeiten der DMA sind in Kapitel 10 des [Reference Manual][] dokumentiert, von besonderem Interesse ist jedoch auch der Abschnitt 2.1 im Kapitel _"System and Memory Overview"_.
